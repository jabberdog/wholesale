$j("#more-views-carousel").owlCarousel({
    autoPlay: 5000,
    items : 4,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [979,4],
    itemsTablet : [770,4],
    itemsMobile : [479, 4],
    touchDrag : true
});