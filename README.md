# [Wholesale LED Lights](https://www.wholesaleledlights.co.uk)
New page layout for the Wholesale LED Lights product page. This includes re-building the structure of the page
using Twitter Bootstrap, Owl Carousel and other resources.

## Owl Carousel Config Options
All javascript parameters and configurations for Owl Carousel can be found at this URL: http://www.owlgraphic.com/owlcarousel/index.html

## How To's
Please use this section to help you set various options on the new product page layout. If you have any questions,
email me [liam@jabberdog.co.uk](mailto:liam@jabberdog.co.uk) or skype me on [jabberdog](skype:-jabberdog-?chat).

### Product Offer
We always have some kind of offer available on the website, and the make the customer aware of this, we usually place
a reminder on the product page in the form of an image. To make this easier for non-technical staff I've allowed the
use of Magento's Static Block system.

**1. Create A Static Block and Add Your Image**
- Go to CMS > Static Blocks > Add New Block
- Give the static block a title
- Add an identifier (which we'll need later so it's important to remember it)
- Select 'All Store Views'
- Use the WYSIWYG editor or paste in the location of the image you want to use (e.g. https://www.wholesaleledlights.co.uk/media/wysiwyg/offerribbon.png)
- Click Save Block

**2. Open or Create a Product**

To set a product offer on the page - add the id of the static block you just created to the field where asked.

#### Example:

Your static block identifier (id) may be called `test-block`, so on editing the product page, where the 'Product Offer'
box is shown, enter `test-block` and save the product. Your image should now show on the product page.

**Note: it is important to keep this method nice and tidy, so use names that describe what content the block is showing.**

Here are the blocks that we have already pre-set (they may be required to be added to the attribute set):

**1. Product Offer Top**
- **Attribute Code:** `wled_custom_block_1`
- **Image Size (W x H):** 360 x 54px
- **Position on Product Page:** Above the product pricing

**2. Product Offer Side**
- **Attribute Code:** `wled_custom_block_2`
- **Image Size (W x H):** 360 x 360px
- **Position on Product Page:** On the side of the page

You are welcome to add more but I would advise keeping the same naming convention to keep it tidy.

### 'More Views' Carousel

The `product-img-box` div contains all the contents for the product images. This also means it contains some of the internal
functions of how it works. You can learn more by looking into files such as `/app/design/frontend/rwd/product/template/catalog/product/view/media.phtml`.

In any case, the most common reason for accessing the carousel is because you may want to configure it differently
(e.g. add more images, remove pagination, speed up auto play time etc). To make it tidier, the settings to do this
are contained within their own Javascript file, here: `/skin/frontend/rwd/product/js/owl.carousel.init.js`.

Parameters can be found here: http://www.owlgraphic.com/owlcarousel/index.html